module.exports = {
  "globDirectory": "public/",
  "globPatterns": [
    "**/*.{html,png,jpg, jpeg,svg,js,css,json,xml}"
  ],
  "swDest": "./public/sw.js",
  "globIgnores": [
    "../workbox-cli-config.js"
  ],
  "runtimeCaching": [{
    urlPattern: new RegExp(".*(?:googleapis|gstatic)\.com.*$"),
    handler: "staleWhileRevalidate",
  }]
};
